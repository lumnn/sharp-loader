# sharp-loader

A webpack loader that utilises sharp library and allows to resize images by putting size in query string

## Getting Started

Start with installing this package and `sharp` library

`npm install --save-dev @lumnn/sharp-loader sharp`

`sharp-loader` just passes the image through `sharp` library and performs actions on it (currently only resize). It still requires a `file-loader` or something similar to output file.

Add this to a list of loaders in your webpack.config.js

```
      {
        test: /\.(png|jpe?g|gif|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name]-[hash:8].[ext]',
            }
          }
        ]
      },
      {
        test: /\.(png|jpe?g|gif|tiff)$/,
        loader: '@lumn/sharp-loader',
      }
```

file-loader should be setup with `hash` placeholder in name to avoid filename conflicts when same image is processed multiple times.

## License

[MIT](LICENSE.md)
