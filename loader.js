const loaderUtils = require('loader-utils')
const sharp = require('sharp')

const defaultOptions = {
  width: null,
  height: null,
}

function stringToNumber (value) {
  if (typeof value === 'string') {
    return +value
  }

  return value
}

module.exports = function sharpWebpackLoader (source, sourceMaps, meta) {
  const options = Object.assign(
    {},
    defaultOptions,
    loaderUtils.getOptions(this),
    loaderUtils.parseQuery(this.resourceQuery)
  )

  const transformer = sharp(source)

  const callback = this.async()

  transformer
    .resize(stringToNumber(options.width), stringToNumber(options.height))
    .toBuffer()
    .then(data => {
      callback(null, data, sourceMaps, meta);
    })
}

module.exports.raw = true
